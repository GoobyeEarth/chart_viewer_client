FROM centos:latest

SHELL ["/bin/bash", "-c"]

RUN yum -y update

# Enable Extra Packages for Enterprise Linux (EPEL) for CentOS
RUN     yum install -y epel-release
# Install Node.js and npm
RUN     yum install -y nodejs npm

RUN yum -y upgrade
RUN yum -y install nginx
RUN yum install -y make vim

RUN mkdir /client

WORKDIR /client

